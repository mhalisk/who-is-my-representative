import React, { useState } from "react";

// MUI
import { makeStyles, Theme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Link from "@material-ui/core/Link";

// Types
import { Representative } from "./Types";

// Services
import {
  getRepresentativesByState,
  getSenatorsByState,
} from "./services/RepresentativeService";

// Modules
import { representativeOptions, stateOptions } from "./SelectOptions";

const useStyles = makeStyles((theme: Theme) => ({
  "@global": {
    body: {
      backgroundColor: "#D6D2D2",
    },
  },
  root: {
    margin: "2vw 5vw 0 5vw",
  },
  headerPaper: {
    padding: "3vw 1vw 3vw 1vw",
    height: 250,
    width: 700,
  },
  select: {
    width: 300,
  },
  accordion: {
    marginTop: "1vw",
    width: 700,
    padding: "1vw",
  },
  table: {
    width: "100%",
    height: "100%",
  },
}));

export default function App() {
  // ***** HOOKS *****
  const classes = useStyles();

  // ***** STATE *****
  const [selectedRepresentativeType, setSelectedRepresentativeType] =
    useState<string>("");
  const [selectedState, setSelectedState] = useState<string>("");
  const [representatives, setRepresentatives] = useState<Representative[]>([]);

  // ***** FUNCTIONS *****
  const handleSearch = async (
    selectedRepresentativeType: string,
    selectedState: string
  ) => {
    if (!selectedState || !selectedRepresentativeType)
      alert("A representative type and state are required to search.");

    let representatives = [];

    if (selectedRepresentativeType === "representative") {
      representatives = await getRepresentativesByState(selectedState);
    }

    if (selectedRepresentativeType === "senator") {
      representatives = await getSenatorsByState(selectedState);
    }

    return setRepresentatives(representatives);
  };

  const handleClear = () => {
    setSelectedState("");
    setSelectedRepresentativeType("");
    setRepresentatives([]);
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="column"
        spacing={5}
        alignContent="center"
        alignItems="center"
      >
        <Grid item>
          <Typography variant="h3">Who's My Representative</Typography>
        </Grid>
        <Grid item>
          <Typography variant="h6">
            Implementation by Michael Halisky
          </Typography>
        </Grid>
        <Grid item>
          <Paper className={classes.headerPaper}>
            <Grid
              container
              direction="column"
              justify="space-around"
              alignItems="center"
              spacing={3}
            >
              <Grid item>
                <Typography variant="subtitle1" align="center">
                  Select the type of representative, then select the state!
                </Typography>
              </Grid>
              <Grid item>
                <TextField
                  select
                  className={classes.select}
                  variant="outlined"
                  label="Representative"
                  value={selectedRepresentativeType}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setSelectedRepresentativeType(e.target.value);
                  }}
                >
                  {representativeOptions.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item>
                <TextField
                  select
                  className={classes.select}
                  variant="outlined"
                  label="State"
                  value={selectedState}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setSelectedState(e.target.value);
                  }}
                >
                  {stateOptions.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid container justify="center" spacing={6}>
                <Grid item>
                  <Button
                    variant="outlined"
                    onClick={() =>
                      handleSearch(selectedRepresentativeType, selectedState)
                    }
                  >
                    Search
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="outlined" onClick={() => handleClear()}>
                    Clear
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
          {representatives?.map((representative) => (
            <Accordion className={classes.accordion} key={representative.name}>
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Grid container justify="space-between">
                  <Grid item>
                    <Typography>{representative.name} </Typography>
                  </Grid>
                  <Grid item>
                    <Typography>{representative.party} </Typography>
                  </Grid>
                </Grid>
              </AccordionSummary>
              <AccordionDetails>
                <TableContainer className={classes.table}>
                  <Table>
                    <TableBody>
                      <TableRow>
                        <TableCell>
                          <Grid container justify="space-between">
                            <Grid item>
                              <Typography>District</Typography>
                            </Grid>
                            <Grid item>
                              <Typography>{representative.district}</Typography>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <Grid container justify="space-between">
                            <Grid item>
                              <Typography>Phone</Typography>
                            </Grid>
                            <Grid item>
                              <Typography>{representative.phone}</Typography>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <Grid container justify="space-between">
                            <Grid item>
                              <Typography>Office</Typography>
                            </Grid>
                            <Grid item>
                              <Typography>{representative.office}</Typography>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <Grid container justify="space-between">
                            <Grid item>
                              <Typography>Website</Typography>
                            </Grid>
                            <Grid item>
                              <Link href={representative.link}>
                                {representative.link}
                              </Link>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </AccordionDetails>
            </Accordion>
          ))}
        </Grid>
      </Grid>
    </div>
  );
}
