export interface SelectOptions {
  label: string;
  value: string;
}

export interface Representative {
  [key: string]: string;
}
