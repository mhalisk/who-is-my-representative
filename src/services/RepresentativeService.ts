import axios from "axios";

// ***** UTIL *****
const isValidResponse = (response: any) => {
  if (response?.status !== 200) return false;
  if (response?.data?.success !== true) return false;
  if (response?.statusText !== "OK") return false;

  return true;
};

const stateHasSenators = (state: string) => {
  if (state === "GU") return false;
  if (state === "DC") return false;
  if (state === "AS") return false;
  if (state === "PR") return false;
  if (state === "VI") return false;

  return true;
};

// ***** SERVICES *****
export const getRepresentativesByState = async (state: string) => {
  try {
    const response = await axios.get(
      `http://localhost:3000/representatives/${state}`
    );

    if (!isValidResponse(response)) {
      const error = new Error("response was invalid");
      alert(error);
      throw error;
    }

    if (response.data.results.length) return response.data.results;

    return [];
  } catch (error) {
    alert(error);
  }
};

export const getSenatorsByState = async (state: string) => {
  try {
    if (!stateHasSenators(state)) {
      alert(`This location does not have senators`);
      return [];
    }

    const response = await axios.get(`http://localhost:3000/senators/${state}`);

    if (!isValidResponse(response)) {
      const error = new Error("response was invalid");
      alert(error);
      throw error;
    }

    if (response.data.results.length) return response.data.results;

    return [];
  } catch (error) {
    alert(error);
  }
};
